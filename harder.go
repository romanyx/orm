package main

/*
type Payout struct {
	Amount   int
	Currency string
}

func (p Payout) Table() string {
	return "payouts"
}

func (p Payout) Fields() []string {
	fields := []string{
		"amount",
		"currency",
	}

	return fields
}

func (p Payout) Values() []interface{} {
	values := []interface{}{
		&p.Amount,
		&p.Currency,
	}

	return values
}

type Table interface {
	Table() string
	Fields() []string
	Values() []interface{}
}

type DB[C any] struct {
	Sql C
}

func NewQueryer[C any, T any](db *DB[C]) *Queryer[C, T] {
	var r T
	q := Queryer[C, T]{
		DB:       db,
		Resource: r,
	}

	return &q
}

type Queryer[C any, T any] struct {
	DB       *DB[C]
	Resource T
}

type ExecerContext interface {
	ExecContext(context.Context, string, ...any) (sql.Result, error)
}

type Conn interface {
	Begin() (*sql.Tx, error)
}

func (q *Queryer[C, T]) Txed(
	ctx context.Context,
	block func(context.Context, *Queryer[*sql.Tx, T]) error,
) error {
	conn := any(q.DB.Sql).(Conn)
	tx, err := conn.Begin()
	if err != nil {
		return err
	}

	nq := NewQueryer[*sql.Tx, T](&DB[*sql.Tx]{
		Sql: tx,
	})

	if err := block(ctx, nq); err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (q *Queryer[C, T]) Store(
	ctx context.Context,
	r *T,
) error {
	table := any(q.Resource).(Table)
	sql, args, err := sq.Insert(table.Table()).
		Columns(table.Fields()...).
		Values(table.Values()...).
		ToSql()
	if err != nil {
		return err
	}

	execer := any(q.DB.Sql).(ExecerContext)
	if _, err := execer.ExecContext(ctx, sql, args...); err != nil {
		return err
	}

	return nil
}

type QueryerContext interface {
	QueryContext(context.Context, string, ...any) (*sql.Rows, error)
}

func (q *Queryer[C, T]) Where(
	ctx context.Context,
	pred interface{},
) ([]T, error) {
	table := any(q.Resource).(Table)
	query, args, err := sq.Select(table.Fields()...).
		From(table.Table()).
		Where(pred).
		ToSql()

	if err != nil {
		return nil, err
	}

	queryer := any(q.DB.Sql).(QueryerContext)
	rows, err := queryer.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var result []T
	for rows.Next() {
		var r T
		if err := rows.Scan(table.Values()...); err != nil {
			return nil, err
		}

		result = append(result, r)
	}

	return result, nil
}
*/
