package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	// Allows to create postgres postgres db instance from package.
	_ "github.com/lib/pq"

	"github.com/ory/dockertest/v3"
)

const (
	dockerStartWait = 30 * time.Second
)

type Postgres struct {
	DB          *sql.DB
	DSN         string
	Resource    *dockertest.Resource
	connTimeout time.Duration
	host        string
}

func newPostgres(pool *dockertest.Pool) (*Postgres, error) {
	res, err := pool.Run("postgres", "latest", []string{
		"POSTGRES_PASSWORD=test",
		"POSTGRES_USER=test",
		"POSTGRES_DB=test",
	})
	if err != nil {
		return nil, fmt.Errorf("start postgres: %w", err)
	}

	purge := func() {
		_ = pool.Purge(res)
	}

	errChan := make(chan error)
	done := make(chan struct{})

	pd := Postgres{
		Resource:    res,
		connTimeout: dockerStartWait,
		host:        "localhost",
	}

	d := os.Getenv(`DOCKER_HOST`)
	if len(d) > 0 {
		pd.host = `docker`
	}

	pd.DSN = fmt.Sprintf("user=test password=test dbname=test host=%s port=%s sslmode=disable", pd.host, res.GetPort("5432/tcp"))

	go func() {
		if err := pool.Retry(func() error {
			pd.DB, err = sql.Open("postgres", pd.DSN)
			if err != nil {
				return err
			}
			if err := pd.DB.Ping(); err != nil {
				pd.DB.Close() // nolint: errcheck
				return err
			}
			return nil
		}); err != nil {
			errChan <- err
		}

		close(done)
	}()

	select {
	case err := <-errChan:
		purge()
		return nil, fmt.Errorf("check connection: %w", err)
	case <-time.After(pd.connTimeout):
		purge()
		return nil, errors.New("timeout on checking postgres connection")
	case <-done:
		close(errChan)
	}

	return &pd, nil
}

const seed = `
CREATE TABLE IF NOT EXISTS payouts (
    id          SERIAL PRIMARY KEY,

    currency    VARCHAR (256) NOT NULL,
    amount      INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
`

func prepareDB(pool *dockertest.Pool) *Postgres {
	docker, err := newPostgres(
		pool,
	)
	if err != nil {
		log.Fatalf("start postgres image: %v", err)
	}

	if _, err := docker.DB.Exec(seed); err != nil {
		log.Fatalf("failed to seed db: %v", err)
	}

	return docker
}
