package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"reflect"
	"strings"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/ory/dockertest/v3"
)

var psql = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

func main() {
	// Database setup.

	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("create new pool: %v", err)
	}

	pgDocker := prepareDB(pool)
	defer func() {
		pgDocker.DB.Close()
		pool.Purge(pgDocker.Resource)
	}()

	// Example start.

	payouts := NewRepository[Payout](pgDocker.DB)

	p := Payout{
		Amount:   1000,
		Currency: "EUR",
	}
	if err := payouts.Store(context.Background(), &p); err != nil {
		log.Print(err)
	}

	payout, err := payouts.Select(context.Background(), sq.Eq{"id": 1})
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("%+v\n", payout)

	payout.Amount = 2000
	if err := payouts.Update(context.Background(), payout); err != nil {
		log.Print(err)
	}

	payouts, err := payouts.Where(context.Background(), sq.Eq{"amount": 2000})
	if err != nil {
		log.Print(err)
	}
	fmt.Printf("%+v\n", payouts)
}

type Payout struct {
	ID       int64  `db:"id,skip_insert"`
	Amount   int64  `db:"amount"`
	Currency string `db:"currency"`

	CreatedAt time.Time `db:"created_at,skip_insert"`
	UpdatedAt time.Time `db:"updated_at,skip_first_insert"`

	table bool `db:"table_name,payouts"`
}

func NewRepository[T any](db *sql.DB) *Repository[T] {
	var r T
	repo := Repository[T]{
		db:     db,
		record: generateRecord[T](&r),
	}

	return &repo
}

type Repository[T any] struct {
	db     *sql.DB
	record *record
}

func (repo *Repository[T]) Update(
	ctx context.Context,
	r *T,
) error {
	record := generateRecord(r)
	sql, args, err := psql.Insert(repo.record.table).
		Columns(record.updateFields...).
		Values(record.updateValues...).
		ToSql()
	if err != nil {
		return err
	}

	if _, err := repo.db.ExecContext(ctx, sql, args...); err != nil {
		return err
	}

	return nil
}

func (repo *Repository[T]) Store(
	ctx context.Context,
	r *T,
) error {
	record := generateRecord(r)
	sql, args, err := psql.Insert(repo.record.table).
		Columns(record.insertFields...).
		Values(record.insertValues...).
		ToSql()
	if err != nil {
		return err
	}

	if _, err := repo.db.ExecContext(ctx, sql, args...); err != nil {
		return err
	}

	return nil
}

func (repo *Repository[T]) Select(
	ctx context.Context,
	pred interface{},
) (*T, error) {
	query, args, err := psql.Select(repo.record.selectFields...).
		From(repo.record.table).
		Where(pred).
		ToSql()

	if err != nil {
		return nil, err
	}

	var r T
	record := generateRecord(&r)
	if err := repo.db.
		QueryRowContext(ctx, query, args...).
		Scan(record.selectValues...); err != nil {
		return nil, err
	}

	return &r, nil
}

func (repo *Repository[T]) Where(
	ctx context.Context,
	pred interface{},
) ([]T, error) {
	query, args, err := psql.Select(repo.record.selectFields...).
		From(repo.record.table).
		Where(pred).
		ToSql()

	if err != nil {
		return nil, err
	}

	rows, err := repo.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var result []T
	for rows.Next() {
		var r T
		record := generateRecord(&r)
		if err := rows.Scan(record.selectValues...); err != nil {
			return nil, err
		}

		result = append(result, r)
	}

	return result, nil
}

type record struct {
	table        string
	insertFields []string
	insertValues []interface{}

	selectFields []string
	selectValues []interface{}

	updateFields []string
	updateValues []interface{}
}

func generateRecord[T any](in *T) *record {
	var r record

	t := reflect.TypeOf(*in)
	v := reflect.ValueOf(in).Elem()
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		value := v.Field(i)
		tag := field.Tag.Get("db")
		parts := strings.Split(tag, ",")
		if len(parts) > 2 {
			panic("db tag has too many parts")
		}

		if parts[0] == "table_name" && field.Name == "table" {
			if len(parts) != 2 {
				panic("table name missing")
			}

			r.table = parts[1]

			continue
		}

		if len(parts) == 2 {
			r.selectValues = append(r.selectValues, value.Addr().Interface())
			r.selectFields = append(r.selectFields, parts[0])

			if parts[1] != "skip_insert" && parts[1] != "skip_first_insert" {
				r.insertFields = append(r.insertFields, parts[0])
				r.insertValues = append(r.insertValues, value.Addr().Interface())
			}

			if parts[1] != "skip_insert" {
				r.updateFields = append(r.updateFields, parts[0])
				r.updateValues = append(r.updateValues, value.Addr().Interface())
			}

			continue
		}

		r.selectValues = append(r.selectValues, value.Addr().Interface())
		r.selectFields = append(r.selectFields, parts[0])
		r.insertFields = append(r.insertFields, parts[0])
		r.insertValues = append(r.insertValues, value.Addr().Interface())
		r.updateFields = append(r.updateFields, parts[0])
		r.updateValues = append(r.updateValues, value.Addr().Interface())
	}

	return &r
}
